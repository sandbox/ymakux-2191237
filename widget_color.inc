<?php

/**
 * @file
 *
 */

/**
 * Color Picker widget.
 */
class FacetapiWidgetColor extends FacetapiWidgetLinks {

  public function execute() {
    $element = &$this->build[$this->facet['field alias']];
    $element = array(
      '#theme' => 'facetapi_color_widget',
      '#items' => $this->buildListItems($element),
      '#attributes' => $this->build['#attributes'],
    );
  }

  function buildListItems($build) {
    $settings = $this->settings->settings;
    $attributes = ($settings['nofollow']) ? array('rel' => 'nofollow') : array();
    $items = array();
    foreach ($build as $value => $item) {
      $variables = array(
        'color' => $item['#markup'],
        'path' => $item['#path'],
        'count' => $item['#count'],
        'options' => array(
          'attributes' => $attributes,
          'html' => $item['#html'],
          'query' => $item['#query'],
        ),
      );

      if (!$item['#count']) {
        $variables['options']['attributes']['class'][] = 'facetapi-zero-results';
      }

      $variables['options']['attributes']['id'] = drupal_html_id('facetapi-link');
      $class = ($item['#active']) ? 'facetapi-active' : 'facetapi-inactive';
      $variables['options']['attributes']['class'][] = $class;
      $row['variables'] = $variables;
      $items[] = $row;
    }
 
    return $items;
  }
}
