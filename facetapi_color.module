<?php

/**
 * Implementation of hook_apachesolr_field_mappings().
 */
function facetapi_color_apachesolr_field_mappings() {
  $mappings = array(
    'color_field_rgb' => array(
      'indexing_callback' => 'facetapi_color_indexing_callback',
      'facets' => TRUE,
      'facet missing allowed' => TRUE,
    ),
  );
  return $mappings;
}

function facetapi_color_indexing_callback($entity, $field_name, $index_key, $field_info) {
  $fields = array();
  if (!empty($entity->{$field_name})) {
    $field = $entity->$field_name;
    list($lang, $values) = each($field);
    for ($i = 0; $i < count($values); $i++) {
      $color = str_replace('#', '', $values[$i]['rgb']);
      if(facetapi_color_hex_validate($color)) {
        $fields[] = array(
          'key' => $index_key,
          'value' => $color,
        );
      }
    }

    if ($field_info['multiple'] && !empty($values[0])) {
      $singular_field_info = $field_info;
      $singular_field_info['multiple'] = FALSE;
      $single_key = apachesolr_index_key($singular_field_info);
      $color = str_replace('#', '', $values[0]['rgb']);
      if(facetapi_color_hex_validate($color)) {
        $fields[] = array(
          'key' => $single_key, 
          'value' => $color,
        );
      }
    }
  }
  
  // Dcart module integration
  if(module_exists('dcart') && !empty($entity->{DCART_ATTRIBUTES_FIELD_NAME}[LANGUAGE_NONE])) {
    foreach($entity->{DCART_ATTRIBUTES_FIELD_NAME}[LANGUAGE_NONE] as $delta => $item) {
      if(isset($item['data']['fields'][$field_name])) {
        $color = str_replace('#', '', $item['data']['fields'][$field_name]);
        if(facetapi_color_hex_validate($color)) {
          $fields[] = array(
            'key' => $index_key,
            'value' => $color,
          );
        }
      }
    }
  }
  
  return $fields;
}

function facetapi_color_hex_validate($string) {
  return preg_match('/^[a-f0-9]{6}$/i', $string);
}

/**
 * Implements hook_facetapi_widgets().
 */
function facetapi_color_facetapi_widgets() {
  return array(
    'facetapi_color' => array(
      'handler' => array(
        'label' => t('Color Picker'),
        'class' => 'FacetapiWidgetColor',
        'query types' => array('term'),
      ),
    ),
  );
}

function facetapi_color_theme() {
  return array(
    'facetapi_color_widget' => array(
      'variables' => array('items' => array(), 'attributes' => array()),
    ),
  );
}

function theme_facetapi_color_widget($variables) {

  $items = array();
  foreach ($variables['items'] as $item) {
    $url = url($item['variables']['path'], array(
      'query' => !empty($item['variables']['options']['query']) ? $item['variables']['options']['query'] : array()));
    $element = array(
      '#value' => $item['variables']['color'],
      '#tag' => 'a',
      '#attributes' => array(
        'href' => $url,
        'style' => 'color:#' . $item['variables']['color'] . ';background-color:#' . $item['variables']['color'] . ';',
      ),
    );
    $items[] = theme('html_tag', array('element' => $element));
  }
  return theme('item_list', array('items' => $items, 'attributes' => array('class' => array('facet-color'))));
}
